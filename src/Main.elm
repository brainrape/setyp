module Main exposing (main)

import Html exposing (Html, program, beginnerProgram, div, ul, li, text, button, input)
import Html.Attributes exposing (class, value)
import Html.Events exposing (onInput, onClick)
import List.Extra exposing (removeAt, updateAt, findIndex)
import List exposing (map, filter, indexedMap, take, drop, head)


type alias TypDef =
    { name : String
    , arguments : List String
    , constructors : List ( String, List Typ )
    }


type Typ
    = TypConstructor String (List Typ)
    | TypVar String
    | TypFun Typ Typ
    | TypRecord (List ( String, Typ ))


tFizzBuzz : TypDef
tFizzBuzz =
    TypDef "FizzBuzz"
        []
        [ ( "Fizz", [] )
        , ( "Buzz", [] )
        , ( "FizzBuzz", [] )
        , ( "Number", [ TypConstructor "Int" [] ] )
        ]


type TypDefMsg
    = Rename String
    | AddTypArgument String
    | RemoveTypArgument String
    | RenameTypArgument String String
    | SwapTypArgument String
    | AddConstructor String
    | RemoveConstructor String
    | RenameConstructor String String
    | SwapConstructor String
    | AddConstructorArg String Typ
    | RemoveConstructorArg String Int
    | ChangeConstructorArg String Int Typ
    | SwapConstructorArg String Int


type Msg
    = UpdateTypDef TypDefMsg
    | ChangeInput String


type alias Model =
    { typDef : TypDef
    , input : String
    }


model : Model
model =
    { typDef = tFizzBuzz, input = "Foo" }


insertAt : Int -> a -> List a -> List a
insertAt index element list =
    List.take index list ++ (element :: List.drop index list)


updateTypDef : TypDefMsg -> TypDef -> TypDef
updateTypDef msg model =
    case msg of
        Rename str ->
            { model | name = str }

        AddTypArgument arg ->
            { model | arguments = model.arguments ++ [ arg ] }

        RemoveTypArgument arg ->
            { model | arguments = model.arguments |> filter ((/=) arg) }

        RenameTypArgument old new ->
            { model
                | arguments =
                    model.arguments
                        |> map
                            (\a ->
                                if a == old then
                                    new
                                else
                                    a
                            )
            }

        SwapTypArgument arg ->
            model

        AddConstructor str ->
            { model | constructors = model.constructors ++ [ ( str, [] ) ] }

        RemoveConstructor str ->
            { model | constructors = model.constructors |> filter (\( n, t ) -> n /= str) }

        RenameConstructor old new ->
            { model
                | constructors =
                    model.constructors
                        |> map
                            (\( n, t ) ->
                                if n == old then
                                    ( new, t )
                                else
                                    ( n, t )
                            )
            }

        SwapConstructor str ->
            case findIndex (\( n, t ) -> n == str) model.constructors of
                Just i ->
                    if List.length model.constructors > i + 1 then
                        { model
                            | constructors =
                                []
                                    ++ (model.constructors |> take i)
                                    ++ (model.constructors |> drop (i + 1) |> take 1)
                                    ++ (model.constructors |> drop i |> take 1)
                                    ++ (model.constructors |> drop (i + 2))
                        }
                    else
                        model

                Nothing ->
                    model

        AddConstructorArg con typ ->
            { model
                | constructors =
                    model.constructors
                        |> map
                            (\( n, t ) ->
                                if con == n then
                                    ( n, t ++ [ typ ] )
                                else
                                    ( n, t )
                            )
            }

        RemoveConstructorArg con i ->
            { model
                | constructors =
                    model.constructors
                        |> map
                            (\( n, t ) ->
                                if con == n then
                                    ( n, t |> removeAt i )
                                else
                                    ( n, t )
                            )
            }

        ChangeConstructorArg con i typ ->
            { model
                | constructors =
                    model.constructors
                        |> map
                            (\( n, t ) ->
                                if con == n then
                                    ( n, updateAt i (always typ) t |> Maybe.withDefault t )
                                else
                                    ( n, t )
                            )
            }

        SwapConstructorArg con i ->
            { model
                | constructors =
                    model.constructors
                        |> map
                            (\( n, t ) ->
                                ( n
                                , if n /= con then
                                    t
                                  else
                                    (if List.length model.constructors > i + 1 then
                                        []
                                            ++ (t |> take i)
                                            ++ (t |> drop (i + 1) |> take 1)
                                            ++ (t |> drop i |> take 1)
                                            ++ (t |> drop (i + 2))
                                     else
                                        t
                                    )
                                )
                            )
            }


update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateTypDef m ->
            { model | typDef = updateTypDef m model.typDef }

        ChangeInput str ->
            { model | input = str }


view : Model -> Html Msg
view ({ typDef } as model) =
    div []
        [ div []
            [ div [] [ text "Name: " ]
            , div [] [ text typDef.name ]
            , div [] [ text "Arguments: " ]
            , ul [] ((typDef.arguments |> map (\a -> li [] [ text a, button [ onClick (UpdateTypDef (RemoveTypArgument a)) ] [ text "-" ] ])) ++ [ li [] [ button [ onClick (UpdateTypDef (AddTypArgument model.input)) ] [ text "+" ] ] ])
            , div [] [ text "constructors:" ]
            , ul []
                ((typDef.constructors
                    |> map
                        (\( name, args ) ->
                            li []
                                [ button [ onClick (UpdateTypDef (SwapConstructor name)) ] [ text "v" ]
                                , button [ onClick (UpdateTypDef (RemoveConstructor name)) ] [ text "-" ]
                                , text name
                                , ul []
                                    ((args
                                        |> indexedMap
                                            (\j a ->
                                                li []
                                                    [ button [ onClick (UpdateTypDef (SwapConstructorArg name j)) ] [ text "v" ]
                                                    , button [ onClick (UpdateTypDef (RemoveConstructorArg name j)) ] [ text "-" ]
                                                    , text (toString a)
                                                    ]
                                            )
                                     )
                                        ++ [ li [] [ button [ onClick (UpdateTypDef (AddConstructorArg name (TypConstructor model.input []))) ] [ text "+" ] ] ]
                                    )
                                ]
                        )
                 )
                    ++ [ li [] [ button [ onClick (UpdateTypDef (AddConstructor model.input)) ] [ text "+" ] ] ]
                )
            ]
        , input [ onInput ChangeInput, value model.input ] []
        ]


main : Program Never Model Msg
main =
    beginnerProgram { model = model, update = update, view = view }
